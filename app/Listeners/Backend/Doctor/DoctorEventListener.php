<?php

namespace App\Listeners\Backend\Doctor;

use App\Events\Backend\Doctor\DoctorCreated;
use App\Events\Backend\Doctor\DoctorDeleted;
use App\Events\Backend\Doctor\DoctorUpdated;

/**
 * Class DoctorEventListener.
 */
class DoctorEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        logger('Doctor Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        logger('Doctor Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        logger('Doctor Deleted');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            DoctorCreated::class,
            'App\Listeners\Backend\Doctor\DoctorEventListener@onCreated'
        );

        $events->listen(
            DoctorUpdated::class,
            'App\Listeners\Backend\Doctor\DoctorEventListener@onUpdated'
        );

        $events->listen(
            DoctorDeleted::class,
            'App\Listeners\Backend\Doctor\DoctorEventListener@onDeleted'
        );
    }
}
