<?php

namespace App\Models\Auth\Traits\Method;

/**
 * Trait DoctorMethod.
 */
trait DoctorMethod
{
    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->name === config('access.users.admin_doctor');
    }
}
