<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Doctor\DoctorCreated;
use App\Events\Backend\Doctor\DoctorUpdated;
use App\Exceptions\GeneralException;
use App\Models\Auth\Doctor;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

/**
 * Class DoctorRepository.
 */
class DoctorRepository extends BaseRepository
{
    /**
     * DoctorRepository constructor.
     *
     * @param  Doctor  $model
     */
    public function __construct(Doctor $model)
    {
        $this->model = $model;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->with('doctors', 'permissions')
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @throws GeneralException
     * @throws \Throwable
     * @return Doctor
     */
    public function create(array $data): Doctor
    {
        // Make sure it doesn't already exist
        if ($this->doctorExists($data['name'])) {
            throw new GeneralException('A doctor already exists with the name '.e($data['name']));
        }

        if (! isset($data['permissions']) || ! \count($data['permissions'])) {
            $data['permissions'] = [];
        }

        //See if the doctor must contain a permission as per config
        if (config('access.doctors.doctor_must_contain_permission') && \count($data['permissions']) === 0) {
            throw new GeneralException(__('exceptions.backend.access.doctors.needs_permission'));
        }

        return DB::transaction(function () use ($data) {
            $doctor = $this->model::create(['name' => strtolower($data['name'])]);

            if ($doctor) {
                $doctor->givePermissionTo($data['permissions']);

                event(new DoctorCreated($doctor));

                return $doctor;
            }

            throw new GeneralException(trans('exceptions.backend.access.doctors.create_error'));
        });
    }

    /**
     * @param Doctor  $doctor
     * @param array $data
     *
     * @throws GeneralException
     * @throws \Throwable
     * @return mixed
     */
    public function update(Doctor $doctor, array $data)
    {
        if ($doctor->isAdmin()) {
            throw new GeneralException('You can not edit the administrator doctor.');
        }

        // If the name is changing make sure it doesn't already exist
        if ($doctor->name !== strtolower($data['name'])) {
            if ($this->doctorExists($data['name'])) {
                throw new GeneralException('A doctor already exists with the name '.$data['name']);
            }
        }

        if (! isset($data['permissions']) || ! \count($data['permissions'])) {
            $data['permissions'] = [];
        }

        //See if the doctor must contain a permission as per config
        if (config('access.doctors.doctor_must_contain_permission') && \count($data['permissions']) === 0) {
            throw new GeneralException(__('exceptions.backend.access.doctors.needs_permission'));
        }

        return DB::transaction(function () use ($doctor, $data) {
            if ($doctor->update([
                'name' => strtolower($data['name']),
            ])) {
                $doctor->syncPermissions($data['permissions']);

                event(new DoctorUpdated($doctor));

                return $doctor;
            }

            throw new GeneralException(trans('exceptions.backend.access.doctors.update_error'));
        });
    }

    /**
     * @param $name
     *
     * @return bool
     */
    protected function doctorExists($name): bool
    {
        return $this->model
            ->where('name', strtolower($name))
            ->count() > 0;
    }
}
