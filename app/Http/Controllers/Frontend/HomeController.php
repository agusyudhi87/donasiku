<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Donation;
use App\Http\Requests\Frontend\Donation\DonationRequest;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.index');
    }

    public function save(DonationRequest $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:donations',
            'gender'=> 'required',
            'nominal'=> 'required',
        ]);

        // $data = new Donation([
        //     'name' => $request->get('name'),
        //     'email' => $request->get('email'),
        //     'gender' => $request->get('gender'),
        //     'nominal' => $request->get('nominal'),
        // ]);
        
        // $data->save();

        $form_data = array(
            'name' => $request->name,
            'email' => $request->email,
            'gender' => $request->gender,
            'nominal' => $request->nominal,
        );

        // dd($form_data);
        Donation::insert($form_data);

        return Response()->json(['success' => 'Data Added successfully.']);
    }
}
