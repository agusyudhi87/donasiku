<?php

namespace App\Events\Backend\Doctor;

use Illuminate\Queue\SerializesModels;

/**
 * Class DoctorCreated.
 */
class DoctorCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $doctor;

    /**
     * @param $doctor
     */
    public function __construct($doctor)
    {
        $this->doctor = $doctor;
    }
}
