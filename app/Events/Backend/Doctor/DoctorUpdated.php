<?php

namespace App\Events\Backend\Doctor;

use Illuminate\Queue\SerializesModels;

/**
 * Class DoctorUpdated.
 */
class DoctorUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $doctor;

    /**
     * @param $doctor
     */
    public function __construct($doctor)
    {
        $this->doctor = $doctor;
    }
}
