@extends('frontend.layouts.app')

@section('content')
<div class="container" id="first" class="1" style="display:">
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="section_title text-center mb-55">
                <h3><span>Make a Donation</span></h3>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-8">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="javascript:void(0)" method="POST" enctype="multipart/form-data" class="donation_form" id="contact_us">
                {{ csrf_field() }}
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="input_field">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nama Lengkap</span>
                                </div>
                                <input type="text" class="form-control" name="name" aria-describedby="basic-addon1" required>
                            </div>
                        </div>
                        <br>
                        <div class="input_field">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Email</span>
                                </div>
                                <input type="email" class="form-control" name="email" aria-describedby="basic-addon1" required>
                            </div>
                        </div>
                        <br>
                        <div class="input_field">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Jenis Kelamin</span>
                                </div>
                                <select name="gender" id="cars" class="form-control" aria-describedby="basic-addon1" required>
                                    <option value="" disabled selected>Select Jenis Kelamin</option>
                                    <option value="0">Perempuan</option>
                                    <option value="1">Laki-Laki</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="input_field">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nominal Donasi</span>
                                </div>
                                <input type="text" id="dengan-rupiah" name="nominal" class="form-control" name="donation" aria-describedby="basic-addon1" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="donate_now_btn text-center">
                    <input type="submit" id="send_form" value="Donasi Sekarang" class="boxed-btn4">
                </div>
            </div>
        </div>
    </form>
</div>
<div class="container" id="res_message" class="2" style="display:none">
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="section_title text-center mb-55">
                <h3><span>Berhasil Donasi</span></h3>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        Terimakasih atas donasi anda
    </div>
</div>
@endsection
@push('script')
    <script>
        var dengan_rupiah = document.getElementById('dengan-rupiah');
        dengan_rupiah.addEventListener('keyup', function(e)
        {
            dengan_rupiah.value = formatRupiah(this.value, 'Rp. ');
        });
        
        dengan_rupiah.addEventListener('keydown', function(event)
        {
            limitCharacter(event);
        });
        
        /* Fungsi */
        function formatRupiah(bilangan, prefix)
        {
            var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
                split	= number_string.split(','),
                sisa 	= split[0].length % 3,
                rupiah 	= split[0].substr(0, sisa),
                ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
        
        function limitCharacter(event)
        {
            key = event.which || event.keyCode;
            if ( key != 188 // Comma
                && key != 8 // Backspace
                && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
                && (key < 48 || key > 57) // Non digit
                // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
                ) 
            {
                event.preventDefault();
                return false;
            }
        }
        // $("#myform").submit(function(e) {
        //     var url = document.getElementById('myform').action,
        //         name = document.getElementById('name').value,
        //         email = document.getElementById('email').value,
        //         gender = document.getElementById('gender').value,
        //         nominal = document.getElementById('nominal').value;
        //     $.ajax({
        //         type: "POST",
        //         url: url,
        //         data: dataString,
        //         success: function(name)
        //         {
        //             document.getElementById('second').innerHTML = name;
        //         },
        //         complete: function() {
        //         // write it here

        //         }
        //     });

        //     e.preventDefault(); // avoid to execute the actual submit of the form.

        // });

        if ($("#contact_us").length > 0) {
            $("#contact_us").validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 50
                },
                    email: {
                    required: true,
                    maxlength: 50,
                    email: true,
                },    
            },
            messages: {
                name: {
                    required: "Please enter name",
                    maxlength: "Your last name maxlength should be 50 characters long."
                },
                email: {
                    required: "Please enter valid email",
                    email: "Please enter valid email",
                    maxlength: "The email name should less than or equal to 50 characters",
                },
                
            },
            submitHandler: function(form) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#send_form').html('Sending..');
            $.ajax({
                url: '{{ url("donation/save") }}' ,
                type: "POST",
                data: $('#contact_us').serialize(),
                success: function( response ) {
                    $('#send_form').html('Submit');
                    $('#res_message').show();
                    $("#first").hide();
                }
            });
            }
        })
        }
        
        // $(document).ready(function() {
        //     $("#myform").submit(function(e) {
        //         e.preventDefault();
        //         $("#first").hide();
        //         $("#second").show();
        //     });
        // });
    </script>
@endpush
