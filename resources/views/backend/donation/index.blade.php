@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="card-title mb-0">
                                Donation Management
                            </h4>
                        </div><!--col-->
                    </div><!--row-->
                </div><!--card-header-->
                <div class="card-body">
                    
            
                    <div class="row mt-4">
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Nominal</th>
                                        <th>@lang('labels.general.actions')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($donations as $donation)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $donation->name }}</td>
                                                <td>{{ $donation->email }}</td>
                                                <td>{{ $donation->nominal }}</td>
                                                <td>@include('backend.auth.role.includes.actions', ['role' => $donation])</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div><!--col-->
                    </div><!--row-->
                </div><!--card-body-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
