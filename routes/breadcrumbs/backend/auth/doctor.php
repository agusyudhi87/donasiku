<?php

Breadcrumbs::for('admin.auth.doctor.index', function ($trail) {
    $trail->push(__('menus.backend.access.doctors.management'), route('admin.auth.doctor.index'));
});

Breadcrumbs::for('admin.auth.doctor.create', function ($trail) {
    $trail->parent('admin.auth.doctor.index');
    $trail->push(__('menus.backend.access.doctors.create'), route('admin.auth.doctor.create'));
});

Breadcrumbs::for('admin.auth.doctor.edit', function ($trail, $id) {
    $trail->parent('admin.auth.doctor.index');
    $trail->push(__('menus.backend.access.doctors.edit'), route('admin.auth.doctor.edit', $id));
});
